Simple novel downloader

### Supported Sites

- 飘天文学 https://www.ptwxz.com/

Inspired by https://github.com/z-Wind/getNovel. TODO: add support for other websites

### Features

- [X] retry with dumb exponential backoff
- [X] parallel requests with `-j8`
- [ ] cache
- [ ] limit retries
