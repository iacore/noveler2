#![feature(assert_matches)]
#![feature(iter_partition_in_place)]

mod util;
mod srv {
    pub mod ptwxz;
}

use std::time::Duration;

use color_eyre::eyre::{eyre, WrapErr};
use color_eyre::{eyre, Help};
use derivative::Derivative;
use futures_util::StreamExt;
use isahc::HttpClient;
use lazy_static::lazy_static;
use onig::{Captures, Regex};
use scraper::{Selector, StrTendril};
use smol::io::AsyncReadExt;
use tracing::trace_span;
use url::Url;

/// How to download the book info. Provider dependent.
#[derive(Debug, Clone, Copy)]
pub struct BookMetadata {
    pub id: u32,
    pub provider_name: &'static str,
}

#[derive(Debug, Clone)]
pub struct BookInfo {
    pub author: String,
    pub title: String,
    pub chapters: Vec<ChapterInfo>,
}

#[derive(Debug, Clone)]
pub struct ChapterInfo {
    pub name: String,
    pub url: Url,
}

#[derive(Debug, Clone)]
pub struct Book {
    pub metadata: BookMetadata,
    pub author: String,
    pub title: String,
    pub chapters: Vec<Chapter>,
}

#[derive(Debug, Clone)]
pub struct Chapter {
    pub info: ChapterInfo,
    pub content: String,
}

// network stuff below

#[derive(Debug)]
pub struct FetchContext {
    pub metadata: BookMetadata,
    pub http: HttpClient,
}

pub enum FetchError {
    ScrapeError(eyre::Report),
    NetworkError(eyre::Report),
}

type BoxedFuture<'a, T> = std::pin::Pin<Box<dyn std::future::Future<Output = T> + Send + 'a>>;

type FnFetchInfo = for<'a> fn(&'a FetchContext) -> BoxedFuture<'a, Result<BookInfo, FetchError>>;
type FnFetchChapter =
    for<'a> fn(&'a FetchContext, &'a ChapterInfo) -> BoxedFuture<'a, Result<String, FetchError>>;

#[derive(Derivative)]
#[derivative(Debug, Clone, Copy)]
pub struct FetchTocTask {
    pub metadata: BookMetadata,
    #[derivative(Debug = "ignore")]
    pub fetch_info: FnFetchInfo,
    #[derivative(Debug = "ignore")]
    pub fetch_chapter: FnFetchChapter,
}

/// Content provider
///
/// The website that provide the book content
#[derive(Derivative)]
#[derivative(Debug)]
pub struct Provider {
    pub name: &'static str,
    pub regex: &'static Regex,
    #[derivative(Debug = "ignore")]
    pub test: fn(Captures) -> eyre::Result<FetchTocTask>,
}

/// I select provider based on URL of the book
pub struct ProviderRegistry {
    pub matchers: Vec<Provider>,
}

impl Default for ProviderRegistry {
    fn default() -> Self {
        let mut r = Self { matchers: vec![] };
        srv::ptwxz::register(&mut r);
        r
    }
}

impl ProviderRegistry {
    pub fn find_provider(&self, s: &str) -> eyre::Result<Option<FetchTocTask>> {
        for m in &self.matchers {
            let Some(captures) = m.regex.captures(s) else { continue };
            return Ok(Some((m.test)(captures)?));
        }
        Ok(None)
    }
}

pub struct Scheduler {
    pub root: FetchTocTask,
    pub http: HttpClient,
    /// max number of  concurrent requests
    pub concurrency: usize,
}

impl Scheduler {
    pub fn new(root_task: FetchTocTask, concurrency: usize) -> Result<Scheduler, isahc::Error> {
        Ok(Self {
            root: root_task,
            http: HttpClient::new()?,
            concurrency, // todo: make this configurable
        })
    }

    #[tracing::instrument(skip_all)]
    pub async fn run(self) -> eyre::Result<Book> {
        let FetchTocTask {
            metadata,
            fetch_info,
            fetch_chapter,
        } = self.root;
        let ctx = FetchContext {
            metadata,
            http: self.http,
        };

        // todo: cache TOC with HTTP cache

        let _span = trace_span!("fetch_info", ?ctx).entered();
        let BookInfo {
            author,
            title,
            chapters: chapter_infos,
        } = retry(|| fetch_info(&ctx)).await?;
        drop(_span);

        // todo: cache chapters

        let mut results: Vec<_> = smol::stream::iter(chapter_infos.into_iter().map(|info| async {
            let _span = trace_span!("fetch_chapter", ?ctx, ?info).entered();
            let content = retry(|| fetch_chapter(&ctx, &info)).await?;
            drop(_span);
            Ok::<Chapter, eyre::Report>(Chapter { info, content })
        }))
        .buffered(self.concurrency)
        .collect()
        .await;

        let i = results.iter_mut().partition_in_place(|res| res.is_ok());
        let errs = results.split_off(i);
        let oks = results;

        if !errs.is_empty() {
            return Err(errs
                .into_iter()
                .map(|err| err.expect_err("should be all err"))
                .fold(eyre!("failed to fetch some chapters"), |handler, err| {
                    handler // todo: better errors. not sure if this works
                        .note(format!(".root_cause() == {}", err.root_cause()))
                        .note(err)
                }));
        }
        // todo: cache chapters end

        Ok(Book {
            metadata,
            author,
            title,
            chapters: oks.into_iter().map(|e| e.expect("all ok")).collect(),
        })
    }
}

async fn retry<'a, T>(
    attempt_fn: impl Fn() -> BoxedFuture<'a, Result<T, FetchError>>,
) -> Result<T, eyre::Report> {
    let mut dur = Duration::from_secs(1);
    loop {
        match attempt_fn().await {
            Ok(k) => return Ok(k),
            Err(FetchError::ScrapeError(e)) => return Err(e),
            Err(FetchError::NetworkError(e)) => {
                eprintln!("{e}");
            }
        }
        dur *= 2; // dumb exponential backoff
    }
}
