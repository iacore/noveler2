use std::{fs::File, io::Write};

use color_eyre::{eyre, Help};
use tracing::instrument;
use tracing_error::InstrumentResult;
use url::Url;

struct Args {
    url: Url,
    concurrency: usize,
}
const DEFAULT_HTTP_CONCURRENCY: usize = 8;

#[instrument(skip_all)]
fn parse_args(registry: &noveler::ProviderRegistry) -> eyre::Result<Args> {
    use lexopt::prelude::*;

    let mut parser = lexopt::Parser::from_env();
    let arg0 = parser.bin_name().unwrap_or("noveler").to_owned();
    let mut i = 0;
    let mut url: Option<Url> = None;
    let mut concurrency = DEFAULT_HTTP_CONCURRENCY;
    while let Some(arg) = parser.next()? {
        match arg {
            Long("help") | Short('h') => {
                print_usage_then_exit(&arg0, 0);
            }
            Long("concurrency") | Short('j') => {
                concurrency = parser.value()?.parse()?;
            }
            Long("list-providers") | Long("ls") => {
                print_providers(registry);
                std::process::exit(0)
            }
            Value(ref val) => {
                i += 1;
                if i == 1 {
                    url = Some(val.parse()?)
                } else {
                    return Err(arg.unexpected().into());
                }
            }
            _ => return Err(arg.unexpected().into()),
        }
    }

    let Some(url) = url else {
        print_usage_then_exit(&arg0, 1)
    };

    Ok(Args { url, concurrency })
}

fn print_usage_then_exit(arg0: &str, exit_code: i32) -> ! {
let usage = format!("\
Simple Novel Downloader

SYNOPSIS
    {arg0} [OPTIONS] URL
    {arg0} --list-providers
    {arg0} --help

OPTIONS
    -j<n>                   Set max number of concurrent HTTP requests (default={DEFAULT_HTTP_CONCURRENCY})
    -h  --help              Show usage
    --list-providers        List websites supported
");
println!("{usage}");
    std::process::exit(exit_code)
}

fn print_providers(registry: &noveler::ProviderRegistry) {
    println!("list of providers by name, regex");
    for m in &registry.matchers {
        println!("\t{}\t{:?}", m.name, m.regex);
    }
}

#[instrument(skip_all)]
fn install_tracing() {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{fmt, EnvFilter};

    let fmt_layer = fmt::layer().with_target(false);
    let filter_layer = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    if let Err(e) = tracing_subscriber::registry()
        .with(fmt_layer.with_filter(filter_layer))
        .with(ErrorLayer::default())
        .try_init()
    {
        eprintln!("Warning: Failed to setup tracing");
        eprintln!("Warning: {}", e);
    }
}

#[instrument]
fn main() -> eyre::Result<()> {
    install_tracing();
    color_eyre::install()?;
    let registry = noveler::ProviderRegistry::default();
    let args = parse_args(&registry).suggestion("Use --help")?;
    let Some(task) = registry.find_provider(args.url.as_str())? else {
        println!("no providers found for url {:?}", args.url.as_str());
        print_providers(&registry);
        std::process::exit(1)
    };

    let sched = noveler::Scheduler::new(task, args.concurrency)?;
    let book = smol::block_on(async { sched.run().await })?;
    // todo!("decide file name better")
    let filename = format!(
        "《{title}》作者：{author} （{source}）",
        title = &book.title,
        author = &book.author,
        source = &book.metadata.provider_name
    );
    let mut f = File::create(filename)?;
    for ch in book.chapters {
        f.write_all(ch.info.name.as_bytes())?;
        f.write_all("\n\n".as_bytes())?;
        f.write_all(ch.content.as_bytes())?;
        f.write_all("\n\n\n\n\n".as_bytes())?;
    }
    Ok(())
}
