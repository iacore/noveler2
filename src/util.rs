use crate::*;

pub(crate) fn query_one<'a>(
    doc: &'a scraper::Html,
    selector: &scraper::Selector,
) -> eyre::Result<scraper::ElementRef<'a>> {
    doc.select(selector)
        .next()
        .ok_or_else(|| eyre!("expect >=1 elements: {selector:?}"))
}

pub(crate) fn decode_gbk(buf: &[u8]) -> String {
    let mut decoder = encoding_rs::GBK.new_decoder();
    let mut content = vec![0u8; buf.len() / 2 * 3];
    let (res, _total_read, total_written, _had_malformed) =
        decoder.decode_to_utf8(buf, &mut content, true);
    match res {
        encoding_rs::CoderResult::InputEmpty => {}
        encoding_rs::CoderResult::OutputFull => {
            unreachable!("config error: 3/2 times is not enough space. Need to increase it more.")
        }
    }
    content.truncate(total_written);
    unsafe { String::from_utf8_unchecked(content) }
}
