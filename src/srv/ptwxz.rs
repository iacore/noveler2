//! www.ptwxz.com

use crate::util::{decode_gbk, query_one};
use crate::*;

const PROVIDER_NAME: &str = "飄天文學";

pub(crate) fn register(r: &mut crate::ProviderRegistry) {
    lazy_static! {
        static ref RE_BOOKINFO: Regex =
            Regex::new(r"^https://www.ptwxz.com/bookinfo/(\d+)/(\d+).html$").unwrap();
        static ref RE_TOC: Regex =
            Regex::new(r"^https://www.ptwxz.com/html/(\d+)/(\d+)/$").unwrap();
    }
    r.matchers.push(Provider {
        name: PROVIDER_NAME,
        regex: &RE_BOOKINFO,
        test: from_captures,
    });
    r.matchers.push(Provider {
        name: PROVIDER_NAME,
        regex: &RE_TOC,
        test: from_captures,
    });

    fn from_captures(cap: Captures) -> eyre::Result<FetchTocTask> {
        let Ok(id_kurz) = cap.at(1).unwrap().parse() else { return Err(eyre::eyre!("invalid URL: id_kurz not integer")) };
        let Ok(id) = cap.at(2).unwrap().parse() else { return Err(eyre::eyre!("invalid URL: id not integer")) };
        if id / 1000 != id_kurz {
            return Err(eyre::eyre!("invalid URL: id_kurz != id / 1000"));
        }
        Ok(FetchTocTask {
            metadata: BookMetadata {
                id,
                provider_name: PROVIDER_NAME,
            },
            fetch_info: |a| Box::pin(fetch_info(a)),
            fetch_chapter: |a, b| Box::pin(fetch_chapter(a, b)),
        })
    }
}

#[test]
fn test_find_provider() {
    use std::assert_matches::assert_matches;
    let a = ProviderRegistry::default();
    assert_matches!(
        a.find_provider("https://www.ptwxz.com/bookinfo/8/8024.html"),
        Ok(Some(_))
    );
    assert_matches!(
        a.find_provider("https://www.ptwxz.com/bookinfo/8/1024.html"),
        Err(_)
    );
    assert_matches!(
        a.find_provider("leadhttps://www.ptwxz.com/bookinfo/8/8024.html"),
        Ok(None)
    );
    assert_matches!(
        a.find_provider("https://www.ptwxz.com/bookinfo/8/8024.htmltrail"),
        Ok(None)
    );
    assert_matches!(
        a.find_provider("https://www.ptwxz.com/bookinfo/8/8024.ht"),
        Ok(None)
    );
}

fn get_toc_url(book: BookMetadata) -> Url {
    let id = book.id;
    let id_div_1000 = id / 1000;
    format!("https://www.ptwxz.com/html/{id_div_1000}/{id}/")
        .parse()
        .unwrap()
}

fn scrape_toc(doc: scraper::Html, base_url: &Url) -> eyre::Result<BookInfo> {
    lazy_static! {
        static ref SEL_TOC_TITLE: Selector = Selector::parse(".bottom b").unwrap();
        static ref SEL_TOC_AUTHOR: Selector = Selector::parse(".mainbody>.list").unwrap();
        static ref SEL_TOC_CHAPTER: Selector = Selector::parse(".centent a").unwrap();
    }

    let title = query_one(&doc, &SEL_TOC_TITLE)?.text().collect();

    let el_author = query_one(&doc, &SEL_TOC_AUTHOR)?;
    let node_author = (*el_author).value();
    let author = node_author
        .as_text()
        .ok_or_else(|| eyre!("parse author: not text node"))?
        .text
        .strip_prefix("作者：")
        .ok_or_else(|| eyre!("parse author: no expected prefix"))?
        .to_owned();

    let mut chapters = vec![];
    for el_link in doc.select(&SEL_TOC_CHAPTER) {
        let url = el_link
            .value()
            .attr("href")
            .ok_or_else(|| eyre!("parse chapter link: no [href=...] "))?;

        let url = base_url.join(url).context("parse chapter link")?;
        let name = el_link.text().collect();
        chapters.push(ChapterInfo { name, url });
    }

    Ok(BookInfo {
        author,
        title,
        chapters,
    })
}

fn parse_toc_helper(content: &str, url: &Url) -> eyre::Result<BookInfo> {
    let doc = scraper::Html::parse_document(content);
    let book_info = scrape_toc(doc, url)?;
    Ok(book_info)
}

/// fetch [`BookInfo`] from table of content
async fn fetch_info(ctx: &FetchContext) -> Result<BookInfo, FetchError> {
    let url = get_toc_url(ctx.metadata);
    let mut response = ctx
        .http
        .get_async(url.as_str())
        .await
        .map_err(|e| FetchError::NetworkError(e.into()))?;
    let mut buf = Vec::new();
    response
        .body_mut()
        .read_to_end(&mut buf)
        .await
        .map_err(|e| FetchError::NetworkError(e.into()))?;
    let content = decode_gbk(&buf);
    parse_toc_helper(&content, &url).map_err(FetchError::ScrapeError)
}

fn scrape_chapter_text(doc: scraper::Html) -> eyre::Result<String> {
    lazy_static! {
        static ref SEL_HEAD: Selector = Selector::parse("head").unwrap();
    }
    // note: weird choice of putting <p> in <head>
    let el_head = query_one(&doc, &SEL_HEAD)?;
    let mut children = el_head.children();
    // skip garbage in <head>
    for el in children.by_ref() {
        if let Some(el) = el.value().as_element() {
            if &el.name().to_ascii_lowercase() == "br" {
                break;
            }
        }
    }
    let mut content = StrTendril::new();
    for el in children {
        match el.value() {
            scraper::Node::Text(text) => {
                let paragraph = text.text.trim_matches(|c| ['\n', '\r'].contains(&c));
                content.push_tendril(&paragraph.into());
            }
            scraper::Node::Element(el) if &el.name().to_ascii_lowercase() == "br" => {
                content.push_char('\n');
            }
            x => return Err(eyre!("parse article: met weird node {x:?}")),
        }
    }
    // todo: leading space of first paragraph is trimmed
    Ok(content.trim().to_owned())
}

fn parse_chapter(content: &str) -> eyre::Result<String> {
    let doc = scraper::Html::parse_document(content);
    let book_info = scrape_chapter_text(doc)?;
    Ok(book_info)
}

async fn fetch_chapter(ctx: &FetchContext, chapter: &ChapterInfo) -> Result<String, FetchError> {
    let mut response = ctx
        .http
        .get_async(chapter.url.as_str())
        .await
        .map_err(|e| FetchError::NetworkError(e.into()))?;
    let mut buf = Vec::new();
    response
        .body_mut()
        .read_to_end(&mut buf)
        .await
        .map_err(|e| FetchError::NetworkError(e.into()))?;
    let content = decode_gbk(&buf);
    parse_chapter(&content).map_err(FetchError::ScrapeError)
}

#[test]
fn test_parse_all() -> eyre::Result<()> {
    let paths = glob::glob("testset/www.ptwxz.com/html/11/11531/*.html")?;
    for path in paths {
        let path = path?;
        print!("{} ... ", path.to_string_lossy());
        let content = std::fs::read(&path)?;
        let decoded = decode_gbk(&content);
        if path.ends_with("/index.html") {
            let ans = parse_toc_helper(
                &decoded,
                &"https://www.ptwxz.com/html/11/11531/".parse().unwrap(),
            )?;
            println!("{}", ans.chapters.len());
        } else {
            let ans = parse_chapter(&decoded)?;
            println!("{}", ans.len());
        }
    }
    Ok(())
}
